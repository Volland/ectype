"use ectype";

import { Type } from "../../../core/core.js";

// Can't cast a value to a type.
const A = Type.from(10);
