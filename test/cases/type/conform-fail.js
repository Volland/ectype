"use ectype";

import { Type } from "../../../core/core.js";

// Can't call conform() on Type.
const A = Type.conform(10);
