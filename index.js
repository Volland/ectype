"ectype:core";
export {
  Bool,
  Null,
  Num,
  Str,
  Type,
  Unknown,
  array,
  cond,
  fn,
  js,
  struct,
  tuple,
  variant,
} from "./dist/core/core.js";
